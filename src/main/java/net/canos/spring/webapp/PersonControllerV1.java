package net.canos.spring.webapp;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController(value="PersonControllerV1")
@RequestMapping("/v1/person")
@Api(value = "Person v1")
public class PersonControllerV1 {
	Logger log = Logger.getLogger(getClass());

	@Autowired
	private PersonService personService;

	/**
	 * @return personId si <= 0 devuelve null, si no objeto vacío
	 */
	@RequestMapping(value="/{personId}",method= RequestMethod.GET)
	@ApiOperation(value = "Get a person", 
	notes="Get a person from the repository", 
	response = PersonDTO.class)
	public ResponseEntity<?> get(@PathVariable("personId") Integer personId){
		log.info("GET");
		PersonDTO person = personService.findById(personId);
		
		if(person == null) {
			throw new ResourceNotFoundException("Person not found");
		}
		return new ResponseEntity<PersonDTO>(person,HttpStatus.OK);
	}
	
	
	/*
	 * @RequestBody va a hacer quep ueda rellenar el objeto mediante JSON
	 * Deberé configurar postman para indicarle que envía JSON y no text (que es el por defecto)
	 * Deberé enviar una petición POST con Body raw y ahí poner JSON, además indicar que es de tipo application/json
	 * Tal y como haría jQuery por detrás
	 * 
	 * Si no pasa la validación lanzará MethodArgumentNotValidException
	 * Si pasamos BindingResult o Error por parametro, NO lanzará esa excepción
	 */
	@RequestMapping(value="/save",method= RequestMethod.POST)
	public ResponseEntity<?> create(@Valid @RequestBody PersonForm person){
		log.info("POST");
		PersonDTO person_new = personService.create(person);
		
		if(person == null) {
			throw new ResourceNotFoundException("Person not found");
		}
		return new ResponseEntity<PersonDTO>(person_new,HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/{personId}/update",method= RequestMethod.PUT)
	public ResponseEntity<?> save(PersonForm person){
		log.info("PUT");
		PersonDTO person_new = personService.save(person);

		if(person == null) {
			throw new ResourceNotFoundException("Person not found");
		}
		return new ResponseEntity<PersonDTO>(person_new,HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/{personId}/delete",	method	= RequestMethod.DELETE)
	public ResponseEntity<?> delete(@PathVariable("personId") Integer personId){
		log.info("DELETE");
		personService.delete(personId);
		
		return new ResponseEntity<PersonDTO>(HttpStatus.NO_CONTENT);
	}
}
