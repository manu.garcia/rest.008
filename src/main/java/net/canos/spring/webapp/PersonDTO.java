package net.canos.spring.webapp;

import java.util.Date;


public class PersonDTO {
	public enum Gender {MALE, FEMALE};
	
	private Integer id;
	private String name;
	private String surName;
	private Date birthday;
	private Integer height;
	private Integer weight; 
	private String gender; 
	
	public PersonDTO() {
		super();
	}
	
	public PersonDTO(Person p) {
		this.id = p.getId();
		this.name = p.getName();
		this.surName = p.getSurName();
		this.birthday = p.getBirthday();
		this.height = p.getHeight();
		this.weight = p.getWeight();
		this.gender = (p.getGender() != null && p.getGender().equals(Gender.MALE)) ? "male" : "female";
	}
	
	public PersonDTO(PersonForm p) {
		this.id = p.getId();
		this.name = p.getName();
		this.surName = p.getSurName();
		this.birthday = p.getBirthday();
		this.height = p.getHeight();
		this.weight = p.getWeight();
		this.gender = (p.getGender() != null && p.getGender().equals(Gender.MALE)) ? "male" : "female";
	}

	public String getSurName() {
		return surName;
	}
	public void setSurName(String surName) {
		this.surName = surName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public Integer getHeight() {
		return height;
	}
	public void setHeight(Integer height) {
		this.height = height;
	}
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	
	@Override
	public String toString() {
		return "Person [name=" + name + ", surName=" + surName + ", birthday=" + birthday + ", height=" + height
				+ ", weight=" + weight + "]";
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
}
