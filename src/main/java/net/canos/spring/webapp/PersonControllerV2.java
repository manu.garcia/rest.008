package net.canos.spring.webapp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController(value="PersonControllerV2")
@RequestMapping("/person")
@Api(value = "Person v2")
public class PersonControllerV2 {
	Logger log = Logger.getLogger(getClass());

	@Autowired
	private PersonService personService;

	
	
	/**
	 * Sólo accesible si está la cabecera X-API-Version=2
	 * Probar con postman 
	 */
	@RequestMapping(value="/{personId}",method=RequestMethod.GET,headers= {"X-API-Version=2"})
	@ApiOperation(value = "Get a person", 
		notes="Get a person from the repository", 
		response = PersonDTOv2.class)
	public ResponseEntity<?> getV3(@PathVariable("personId") Integer personId) {
		log.info("GET v2");
		PersonDTO person = personService.findById(personId);
		PersonDTOv2 personV2 = new PersonDTOv2(person);
		
		if(person == null) {
			throw new ResourceNotFoundException("Person not found");
		}
		return new ResponseEntity<PersonDTOv2>(personV2,HttpStatus.OK);
	}
	
	/**
	 * Sólo accesible si está la cabecera X-API-Version=2
	 * Probar con postman 
	 */
	@RequestMapping(value="/{personId}",method=RequestMethod.GET)
	public ResponseEntity<?> getV4(@PathVariable("personId") Integer personId) {
		log.info("GET v2");
		PersonDTO person = personService.findById(personId);
		PersonDTOv2 personV2 = new PersonDTOv2(person);
		
		if(person == null) {
			throw new ResourceNotFoundException("Person not found");
		}
		return new ResponseEntity<PersonDTOv2>(personV2,HttpStatus.OK);
	}
	
	/*HATEOAS Example
	@RequestMapping(value="/persons", method=RequestMethod.POST)
	public ResponseEntity<?> createPerson(@RequestBody Person person) {
		PersonDTO personDTO = personService.save(person);
		 
		// Set the location header for the newly created resource
		HttpHeaders responseHeaders = new HttpHeaders();
		URI newPersonUri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(personDTO.getId()).toUri();
		responseHeaders.setLocation(newPersonUri);
		 
		return new ResponseEntity<>(null, responseHeaders, HttpStatus.CREATED);
	} */
}
